#include <iostream>
#include <math.h>
#include <stdio.h>
#include <bitset>
#include "BaseCache.h"
#define DEBUG false
using namespace std;

//WRITE ME
//Default constructor to set everything to '0'
BaseCache::BaseCache() {}

//WRITE ME
//Constructor to initialize cache parameters, create the cache and clears it
BaseCache::BaseCache(uint32_t _cacheSize, uint32_t _associativity, uint32_t _blockSize) :
	cacheSize(_cacheSize), 
	associativity(_associativity),
	blockSize(_blockSize)
{
	this->initDerivedParams();
}

//WRITE ME
//Set cache base parameters
void BaseCache::setCacheSize(uint32_t _cacheSize) {this->cacheSize = _cacheSize;}
void BaseCache::setAssociativity(uint32_t _associativity) {this->associativity = _associativity;}
void BaseCache::setBlockSize(uint32_t _blockSize) {this->blockSize = _blockSize;}

//WRITE ME
//Get cache base parameters
uint32_t BaseCache::getCacheSize() {return this->cacheSize;}
uint32_t BaseCache::getAssociativity() {return this->associativity;}
uint32_t BaseCache::getBlockSize() {return this->blockSize;}

//WRITE ME
//Get cache access statistics
uint32_t BaseCache::getReadHits() { return this->numReadHits;}
uint32_t BaseCache::getReadMisses() { return this->numReadMisses;}
uint32_t BaseCache::getWriteHits() {return this->numWriteHits;}
uint32_t BaseCache::getWriteMisses() {return this->numWriteMisses;}
double BaseCache::getReadHitRate() {return 0;}
double BaseCache::getReadMissRate() {return 0;}
double BaseCache::getWriteHitRate() {return 0;}
double BaseCache::getWriteMissRate() {return 0;}
double BaseCache::getOverallHitRate() {return 0;}
double BaseCache::getOverallMissRate() {return 0;}

//WRITE ME
//Initialize cache derived parameters
void BaseCache::initDerivedParams() {
	this->numBlocks = this->cacheSize / this->blockSize;
	this->numSets = this->numBlocks / this->associativity;
	this->blocksPerSet = this->numBlocks / this->numSets;
	this->setBits = log2(this->numSets);
	this->offBits = log2(this->blockSize);
	this->tagBits = ADDR_BITS - this->setBits - this->offBits;
	
	// arrange the cacheLines. cacheLines[set][block]
	this->cacheLines = (cacheLine***) malloc(this->numSets * sizeof(cacheLine**)); // please work
	for (int i = 0; i < this->numSets; i++){
		this->cacheLines[i] = (cacheLine**) malloc(this->blocksPerSet * sizeof(cacheLine*));
		for (int j = 0; j < this->blocksPerSet; j++){
			this->cacheLines[i][j] = (cacheLine*) malloc(sizeof(cacheLine));
			this->cacheLines[i][j]->data = (uint32_t*) malloc(sizeof(uint32_t));
			*this->cacheLines[i][j]->data = (uint32_t)0x0;
			this->cacheLines[i][j]->tag = (uint32_t)NULL;
			this->cacheLines[i][j]->LRUStackBits = (uint32_t)NULL;
			this->cacheLines[i][j]->valid = false;
		}
	}

//	for (int i = 0; i < this->numBlocks; i++){ // instantiate cache blocks pointers
//		this->cacheLines[i] = (cacheLine*) malloc(sizeof(cacheLine));
//		this->cacheLines[i]->data = (uint32_t*)malloc(sizeof(uint32_t)); // TODO figure out how to set up the data pointer
		
//	}
	if (DEBUG) {
		cout << "# DEBUG # Cache initialized with: " << endl;
		cout << "size " << this->cacheSize << endl;
		cout << "assoc " << this->associativity << endl;
		cout << "blockSize " << this->blockSize << endl;
		cout << "numBlocks " << this->numBlocks << endl;
		cout << "numSets " << this->numSets << endl;
		cout << "blocksPerSet " << this->blocksPerSet << endl;
		cout << "bits : [TAG " << this->tagBits << "] [SET " << this->setBits << "] [OFFSET " << this->offBits << "]" << endl;
		//exit(0);
	}
}

//WRITE ME
//Reset cache access statistics
void BaseCache::resetStats() { // never used
}

//WRITE ME
//Create cache and clear it
void BaseCache::createCache() { // never used
}

//WRITE ME
//Reset cache
void BaseCache::clearCache() { // never used
}

//WRITE ME
//Read data
//return true if it was a hit, false if it was a miss
//data is only valid if it was a hit, input data pointer
//is not updated upon miss. Make sure to update LRU stack
//bits. You can choose to separate the LRU bits update into
// a separate function that can be used from both read() and write().
bool BaseCache::read(uint32_t addr, uint32_t *data) {
	bool hit = false;
	uint32_t offBits = getBitInRange(addr, this->offBits - 1, 0);
	uint32_t setBits = getBitInRange(addr, this->offBits + this->setBits - 1, this->offBits);
	uint32_t tagBits = getBitInRange(addr, ADDR_BITS, this->setBits + this->offBits);
	int block_index = this->lookup(tagBits, (int)setBits);
	if(block_index > -1) hit = true;
	if(hit){
		this->numReadHits++;
	}else{
		this->numReadMisses++;
	}
	if(DEBUG) cout << "READ addr:    " << bitset<32>(addr) << endl <<\
	  	"READ offBits: " << bitset<32>(offBits) << " " << hex << offBits << dec << endl <<\
		"READ setBits: " << bitset<32>(setBits) << " " << hex << setBits << dec << endl <<\
		"READ tagBits: " << bitset<32>(tagBits) << " " << hex << tagBits << dec << endl <<\
		"READ blockIndex: " << (int)block_index << endl << \
		" HIT/MISS :" << (int)hit << endl;

	this->numReads++;
	return hit;
}

//WRITE ME
//Write data
//Function returns write hit or miss status. 
bool BaseCache::write(uint32_t addr, uint32_t data) {
	bool hit = false;
	uint32_t offBits = getBitInRange(addr, this->offBits - 1, 0);
	uint32_t setBits = getBitInRange(addr, this->offBits + this->setBits - 1, this->offBits);
	uint32_t tagBits = getBitInRange(addr, ADDR_BITS, this->setBits + this->offBits);
	int block_index = this->lookup(tagBits, (int)setBits);
	if(block_index > -1) hit = true;
	if(hit){
		this->cacheLines[(int)setBits][block_index]->tag = tagBits;
		*this->cacheLines[(int)setBits][block_index]->data = data;
		this->numWriteHits++;
	}else{
		//TODO handle misses
		//TODO for now instead of lru use last line in set as the block to write
		this->cacheLines[(int)setBits][this->blocksPerSet - 1]->tag = tagBits;
		*this->cacheLines[(int)setBits][this->blocksPerSet - 1]->data = data;
		this->numWriteMisses++;
	}
	if (DEBUG) cout << " WRITING 0x" << hex << tagBits << dec << " to set " << (int)setBits << " index " << this->blocksPerSet - 1 << endl;
	if(DEBUG) cout << "WRITE addr:    " << bitset<32>(addr) << endl <<\
		"WRITE offBits: " << bitset<32>(offBits) << " " << hex << offBits << dec << endl <<\
		"WRITE setBits: " << bitset<32>(setBits) << " " << hex << setBits << dec << endl <<\
	  	"WRITE tagBits: " << bitset<32>(tagBits) << " " << hex << tagBits << dec << endl <<\
		"WRITE blockIndex: " << (int)block_index << endl << \
		" HIT/MISS :" << (int)hit << endl << " --- " << endl;

	this->numWrites++;
	return hit;
}


int BaseCache::lookup(uint32_t tag, int set){
	cacheLine *lru_block;
	uint32_t lru = (uint32_t)0x00;
	int block_index = -1;
	for(int i = 0; i < this->blocksPerSet; i++){ // go through all cachelines in set
		this->cacheLines[set][i]->LRUStackBits = shiftLRUStack(this->cacheLines[set][i]->LRUStackBits); // shift lru
		if(this->cacheLines[set][i]->tag == tag || this->cacheLines[set][i]->valid){ // lookup tag
			if(this->cacheLines[set][i]->LRUStackBits <= lru || lru == (uint32_t)0x00){ // find lru
				block_index = i;
				lru = this->cacheLines[set][i]->LRUStackBits;
				lru_block = this->cacheLines[set][i];
			}
		}
	}
	if(block_index > -1){
		lru_block->LRUStackBits++; // update LRU
	}
	return block_index;
}

//WRITE ME
//Destructor to free all allocated memeroy.
BaseCache::~BaseCache() {
	for(int i = 0; i < this->numSets; i++){
		for(int j = 0; j < this->blocksPerSet; j++){
			free(cacheLines[i][j]->data);
			free(cacheLines[i][j]);
		}
	}
}

uint32_t getBitInRange(uint32_t addr, int msb, int lsb){ 
	uint32_t result = (addr >> lsb) & ~(~0 << (msb-lsb+1));
	return result;

}

uint32_t shiftLRUStack(uint32_t lru){
	return lru << 1 & (uint32_t)0x7f; // shift by 1 and with 01111111
}
